package main

import "unsafe"

func main() {
	addr := unsafe.Pointer(uintptr(0x000111))
	value := (*byte)(addr)
	print(*value)
}
