Well, it is almost like this:

`#GOLANG`

    package main
    
    import "unsafe"
    
    func main() {
	    addr := unsafe.Pointer(uintptr(0x000111))
	    value := (*byte)(addr)
	    print(*value)
    }
    
Note that, in most system, `0x000111` is under protecting, so codes will failed.

I think codes up there will act just same with:
    
`#C++`

    #include <iostream>

    int main() {
        int addr = 0xFA000111;
        char t = *(char *)addr;
        std::cout << t << std::endl;
        return 0;
    }
    
I wonder there is another way to do so, as I thinking must casting two times is so... useless and boring.